#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream fout(logFile);
	int cycles = 0;
	fout << "Processing Job # 0..." << endl;
	while (jobQueue.Size() != 0) {
		if (cycles < 10) {
			fout << "CYCLE" << " " << cycles;
			fout << setw(23) << "REMAINING: " << jobQueue.Front()->fcfs_timeRemaining << endl;
		}
		else if ((cycles > 9) && (cycles < 100)) {
			fout << "CYCLE" << " " << cycles;
			fout << setw(22) << "REMAINING: " << jobQueue.Front()->fcfs_timeRemaining << endl;
		}
		else if ((cycles > 99) && (cycles < 1000)) {
			fout << "CYCLE" << " " << cycles;
			fout << setw(21) << "REMAINING: " << jobQueue.Front()->fcfs_timeRemaining << endl;
		}
		else {
			fout << "CYCLE" << " " << cycles;
			fout << setw(20) << "REMAINING: " << jobQueue.Front()->fcfs_timeRemaining << endl;
		}
		jobQueue.Front()->Work(FCFS);
		if (jobQueue.Front()->fcfs_done) {
			jobQueue.Front()->SetFinishTime(cycles, FCFS);
			jobQueue.Pop();
			if (jobQueue.Size() != 0) {
				fout << endl << "Processing Job # " << jobQueue.Front()->id << "..." << endl;
			}
		}
		cycles++;
	}

	fout << endl << endl << endl;
	
	for (int i = 0; i < allJobs.size(); i++) {
		fout << "Job #" << allJobs[i].id << ":" << endl;
		fout << "Time to complete: " << allJobs[i].fcfs_finishTime << endl;
	}

	fout << endl << endl;
	fout << "Time to complete all jobs: " << cycles << endl;

	int average;
	int total = 0;
	for (int i = 0; i < allJobs.size(); i++) {
		total += allJobs[i].fcfs_finishTime;
	}
	average = total / allJobs.size();
	fout << "Average time to coplete a job: " << average << endl;
	fout.close();
}

void Processor::RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile)
{
	ofstream output(logFile);
	int cycles = 0;
	int timer = 0;
	output << "Processing Job # 0..." << endl;
	while (jobQueue.Size() != 0) {
		if (timer == timePerProcess) {
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
			output << "Processing Job # " << jobQueue.Front()->id << "..." << endl;
		}
		if (cycles <= 9) {
			output << "CYCLE" << " " << cycles;
			output << setw(23) << "REMAINING: " << jobQueue.Front()->rr_timeRemaining << endl;
		}
		else if ((cycles > 9) && (cycles < 100)) {
			output << "CYCLE" << " " << cycles;
			output << setw(22) << "REMAINING: " << jobQueue.Front()->rr_timeRemaining << endl;
		}
		else if ((cycles > 100) && (cycles < 1000)) {
			output << "CYCLE" << " " << cycles;
			output << setw(21) << "REMAINING: " << jobQueue.Front()->rr_timeRemaining << endl;
		}
		else {
			output << "CYCLE" << " " << cycles;
			output << setw(20) << "REMAINING: " << jobQueue.Front()->rr_timeRemaining << endl;
		}
		jobQueue.Front()->Work(RR);
		if (jobQueue.Front()->rr_done) {
			jobQueue.Front()->SetFinishTime(cycles, RR);
			jobQueue.Pop();
			if (jobQueue.Size() != 0) {
				output << "Processing Job # " << jobQueue.Front()->id << "..." << endl;
			}
		}
		timer++;
		cycles++;
	}
	output << endl << endl << endl;
	output << "ROUND ROBIN RESULTS:" << endl;
	output << "JOB ID" << setw(20) << "TIME TO COMPLETE" << setw(20) << "TIMES INTERRUPTED" << endl;
	for (int i = 0; i < allJobs.size(); i++) {
		if (i >= 10) {
			output << allJobs[i].id << setw(24) << allJobs[i].rr_finishTime << setw(20) << allJobs[i].rr_timesInterrupted << endl;
		}
		else {
			output << allJobs[i].id << setw(25) << allJobs[i].rr_finishTime << setw(20) << allJobs[i].rr_timesInterrupted << endl;
		}
	}
	output << endl;
	output << "SUMMARY:" << endl;

	int average;
	int total = 0;
	for (int i = 0; i < allJobs.size(); i++) {
		total += allJobs[i].rr_finishTime;
	}

	average = total / allJobs.size();
	output << "Average time to complete jobs: " << average << endl;
	output << "Total time to complete all jobs: " << cycles << endl;
	output << "Round Robin time interval: " << timePerProcess << endl;

	output.close();
}

#endif
