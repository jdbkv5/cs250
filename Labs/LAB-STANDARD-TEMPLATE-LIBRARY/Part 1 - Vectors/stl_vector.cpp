// Lab - Standard Template Library - Part 1 - Vectors
// FIRSTNAME, LASTNAME

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses;

	bool done = false;
	while (!done) {

		cout << endl << "MAIN MENU" << endl;
		cout << "Vector size: " << courses.size() << endl;
		cout << "1. Add a new course. "
			<< " 2. Remove last course. "
			<< " 3. Display course list. "
			<< " 4. Quit " << endl;

		int choice;
		cin >> choice;

		if (choice == 1) {					// add a new course
			string newCourse;
			cout << "What is the course name? ";
			cin >> newCourse;
			courses.push_back(newCourse);
		}
		else if (choice == 2) {				// remove last course
			courses.pop_back();
		}
		else if (choice == 3) {				// display course list
			for (int i = 0; i < courses.size(); i++) {
				cout << i << ". " << courses[i] << endl;
			}
		}
		else if (choice == 4) {				// quit
			done = true;
		}
	}


    cin.ignore();
    cin.get();
    return 0;
}
