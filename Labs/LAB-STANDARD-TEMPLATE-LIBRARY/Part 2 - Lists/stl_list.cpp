// Lab - Standard Template Library - Part 2 - Lists
// FIRSTNAME, LASTNAME

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states) {
	for (list<string>::iterator it = states.begin();
		it != states.end();
		it++) 
	{
		cout << *it << "\t";
	}
	cout << endl;
}

int main()
{
	list<string> states;

	bool done = false;
	while (!done) {


		cout << "State list size: " << states.size() << endl;
		cout << "1. Add new state to front " << " 2. Add new state to back " << endl;
		cout << "3. Pop front state " << " 4. Pop back state " << endl;
		cout << "5. Continue " << endl;

		int choice;
		cin >> choice;

		if (choice == 1) {
			cout << "ADD NEW STATE TO FRONT" << endl;
			cout << "Enter new state name: ";
			string state;
			cin.ignore();
			getline(cin, state);
			states.push_front(state);
		}

		else if (choice == 2) {
			cout << "ADD NEW STATE TO BACK" << endl;
			cout << "Enter new state name: ";
			string state;
			cin.ignore();
			getline(cin, state);
			states.push_back(state);
		}

		else if (choice == 3) {
			cout << "POP FRONT STATE" << endl;
			cout << states.front() << " removed" << endl;
			states.pop_front();
		}

		else if (choice == 4) {
			cout << "POP BACK STATE" << endl;
			cout << states.back() << " removed" << endl;
			states.pop_back();
		}

		else if (choice == 5) {
			done = true;
		}
	}
	
			cout << "ORIGINAL LIST" << endl;
			DisplayList(states);
			cout << endl;

			cout << "SORTED LIST" << endl;
			states.sort();
			DisplayList(states);
			cout << endl;

			cout << "REVERSE LIST" << endl;
			states.reverse();
			DisplayList(states);
			cout << endl;

    cin.ignore();
    cin.get();
    return 0;
}
