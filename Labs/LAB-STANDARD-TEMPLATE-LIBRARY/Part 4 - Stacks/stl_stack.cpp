// Lab - Standard Template Library - Part 4 - Stacks
// BLUNDELL, JIMMY

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
    stack<string> word;
    bool done = false;
    
    while (!done) {
        
        string choice;
        cout << "Enter the next letter of the word, or UNDO to undo, or DONE to stop: ";
        cin >> choice;
        
        if (choice == "UNDO") {         // remove last letter entered
            word.pop();
        }
        
        else if (choice == "DONE") {            // end while loop
            done = true;
        }
        
        else {
            word.push(choice);          // push letter to top of stack
        }
        
    }
    
    cout << "The word/phrase you entered was: ";
    while (!word.empty()) {
        cout << word.top();
        word.pop();
    }
    
    cin.ignore();
    cin.get();
    return 0;
}