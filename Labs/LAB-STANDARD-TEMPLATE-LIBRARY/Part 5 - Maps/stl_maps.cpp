// Lab - Standard Template Library - Part 5 - Maps
// JIMMY, BLUNDELL

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
    map<char, string> colors;
    colors['r'] = "FFOOOO";
    colors['g'] = "OOFFOO";
    colors['b'] = "OOOOFF";
    colors['c'] = "OOFFFF";
    colors['m'] = "FFOOFF";
    colors['y'] = "FFFFOO";
    
    bool done = false;
    while (!done) {
        
        char choice;
        cout << "Enter a color letter, or 'q' to stop: ";
        cin >> choice;
        
        if (choice == 'q') {            // exit the program
            done = true;
        }
        
        else {
            cout << "Hex: " << colors[choice] << endl;
        }
        
    }

    cin.ignore();
    cin.get();
    return 0;
}
